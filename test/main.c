#include "../include/fec.h"
#include <string.h>
#include <stdio.h>

#define RAND_TEST_INTENSITY (50)

uint8_t parity[32];
uint8_t message[223];
uint8_t tx_frame[255];

int
run_block_shift_test(int len)
{
	for (int noise_l = 1; noise_l <= 16; noise_l += 4) {
		for (int offs = 0; offs <= len - noise_l + 32; offs += 3) {
			//set initial message
			memset(message, 42, len * sizeof(uint8_t));
			//generate parity
			rs_encode(&parity[0], &message[0], len);
			//append message and its parity on a frame
			memcpy(&tx_frame[0], &message[0], len * sizeof(uint8_t));
			memcpy(&tx_frame[len], &parity[0], 32 * sizeof(uint8_t));

			//add a block of noise to the frame
			memset(&tx_frame[offs], 99, noise_l * sizeof(uint8_t));

			rs_decode(&tx_frame[0], len + 32);

			if (memcmp(message, tx_frame, len * sizeof(uint8_t)) != 0) {
				printf("Error\n");
				return 1;
			}
		}
	}
	return 0;
}

int
run_random_tests()
{
	for (int len = 1; len <= 223; len++) {
		memset(message, 121, len * sizeof(uint8_t));
		rs_encode(&parity[0], &message[0], len);
		memcpy(&tx_frame[0], &message[0], len * sizeof(uint8_t));
		memcpy(&tx_frame[len], &parity[0], 32 * sizeof(uint8_t));
		for (int j = 0; j < 16; j++) {
			tx_frame[((j + 42 * (j + 1 + len))*j) % len] = (j + 42 *
			                (j + 1 + len * 9)) % 255;
			//printf("set %d to %d\n", ((j+42*(j+1+len))*j)%len, (j+42*(j+1+len*9))%255);
		}
		rs_decode(&tx_frame[0], len + 32);
		if (memcmp(message, tx_frame, len * sizeof(uint8_t)) != 0) {
			return 1;
		}
	}
	return 0;
}

int
run_block_tests()
{
	for (int i = 16; i < 223; i = i + 11) {
		if (run_block_shift_test(i) != 0) {
			return 1;
		}
	}
	return 0;
}

int
main()
{

	//This suite will add a variable size block of noise in different frame sizes and offsets.
	if (run_block_tests() != 0) {
		return 1;
	}

	//This suite adds random noise of variable intensity in different frame sizes.

	//This is a non-deterministic test, you can configure it's intensity by changing RAND_TEST_INTENSITY
	for (int i = 0; i < RAND_TEST_INTENSITY; i++) {
		if (run_random_tests() != 0) {
			return 1;
		}
	}

	return 0;
}
